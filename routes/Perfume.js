const router = require('express').Router();
const Perfume = require('../Models/Perfume');
const multer = require('multer');
const path = require('path');
const uuid = require('uuid/v4');
const fs = require('fs-extra');
const cloudinary = require('cloudinary').v2;
const { registerValidation } = require('../Validations/PerfumeValidations');

cloudinary.config({
    cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET
});

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './imgPerfumes/');
    },
    filename: function (req, file, cb) {
        cb(null, uuid() + path.extname(file.originalname).toLowerCase());
    }
});

const fileFilter = (req, file, cb) => {
    const filesTypes = /jpeg|jpg|png|gif/;

    const extname = filesTypes.test(path.extname(file.originalname).toLowerCase());

    const mimetype = filesTypes.test(file.mimetype);

    if (mimetype && extname) {
        cb(null, true);
    } else {
        cb('Error image only!', false);
    }
}

const upload = multer({
    storage,
    limits: { fileSize: 1024 * 1024 * 5 },
    fileFilter
});



router.get('/DownloadPicture/:perfumeId', async (req, res) => {
    const perfume = await Perfume.findById(req.params.perfumeId)
    res.send(perfume.picture); // Set disposition and send it.
});


router.get('/:perfumeId', async (req, res) => {
    try {
        const perfume = await Perfume.findById(req.params.perfumeId)
            .select("name _id picture")
            .populate({ path: 'brand', select: 'name' })
            .populate({ path: 'gender', select: 'name' });
        res.status(200).json({
            perfume:  {
                    _id: perfume._id,
                    Perfume: perfume.name,
                    Brand: perfume.brand.name,
                    Gender: perfume.gender.name,
                    picture: perfume.picture
                }
           
        });
    } catch (error) {
        res.status(404).json({status:404,error});
    }
});

router.get('/', async (req, res) => {
    try {
        const perfume = await Perfume.find()
            .select("name _id picture")
            .populate({ path: 'brand', select: 'name' })
            .populate({ path: 'gender', select: 'name' });
        res.status(200).json({
            count: perfume.length,
            perfumes: perfume.map(p => {
                return {
                    _id: p._id,
                    Perfume: p.name,
                    Brand: p.brand.name,
                    Gender: p.gender.name,
                    picture: p.picture
                }

            })
        });

    } catch (error) {
        res.status(400).send(error)
    }
})
router.post('/register', upload.single('picture'), async (req, res) => {

    const { error } = registerValidation(req.body);

    const perfumeExist = await Perfume.findOne({ name: req.body.name, brand: req.body.brand });

    if (error) return res.status(400).send(error.details[0].message);

    if (!req.file) return res.status(400).send('just images.');

    if (perfumeExist) return res.status(400).send('Perfume already exists.');

    try {
        const cloudFile = await cloudinary.uploader.upload(req.file.path);
        const perfume = new Perfume({
            name: req.body.name,
            brand: req.body.brand,
            gender: req.body.gender,
            picture: cloudFile.secure_url
        });
        await perfume.save();
        await fs.unlink(req.file.path);
        res.status(200).json({ perfume: perfume._id });
    } catch (error) {
        res.status(400).send(error)
    }
})


module.exports = router;