const router = require('express').Router();
const Brand = require('../Models/Brand');
const {registerValidation} = require('../Validations/BrandValidations');

router.get('/', async (req, res) => {
    try {
        const brand = await Brand.find();
        res.json(brand);
    } catch (error) {
        res.json(error);
    }
});

router.post('/register', async (req, res) => {
    const {error} = registerValidation(req.body);

    if (error) return res.status(400).send(error.details[0].message);

    const brandExist = await Brand.findOne({name:req.body.name});

    if (brandExist) return res.status(400).send('Brand already exists.');
    
    try {
        const brand = new Brand({
            name: req.body.name
        });
        const savedbrand = await brand.save();
        res.send({ brand: brand._id });
    } catch (error) {
        res.status(404).send(error);
    }
});

module.exports = router;
