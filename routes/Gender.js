const router = require('express').Router();
const Gender = require('../Models/Gender');

router.get('/', async (req,res) => {
    try {
    const genders = await Gender.find();
    res.json(genders);        
    } catch (error) {
        res.json(error);
    }
});

router.post('/register', async (req,res) => {
    try {
    const gender =new Gender({
        name: req.body.name
    });
    const genderCreated  = await gender.save()
    res.json({gender: gender._id});        
    } catch (error) {
        res.status(400).send(error)
    }
});

module.exports = router;