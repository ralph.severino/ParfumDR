const Joi = require('@hapi/joi');


const registerValidation = (data) => {
    const schema = Joi.object({
        name: Joi.string()
              .min(2)  
              .required()
    });
    return schema.validate(data);
};



module.exports.registerValidation = registerValidation;