const Joi = require('@hapi/joi');

const registerValidation = ( data ) => {
    const schema = Joi.object({
        name: Joi.string()
        .min(2)
        .max(255)  
        .required(),

        brand: Joi.string()
        .required(),

        gender: Joi.string()
        .required()
    });
    return schema.validate(data);
}

module.exports.registerValidation = registerValidation;
