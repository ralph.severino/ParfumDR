const express = require('express');
const app =  express();
const dotenv = require('dotenv');
dotenv.config();
// Import routes

const brandRoute = require('./routes/Brand');
const genderRoute = require('./routes/Gender');
const perfumsRoute = require('./routes/Perfume');

//Middlewares
app.set('port',(process.env.PORT || 3500));
app.use(express.json());

// Database configuration
const mongoose = require('mongoose');

mongoose.connect(process.env.DB_CONNECT,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    dbName:'parfumdr'
},
() => console.log("PARFUM SERVER IS CONNECTED"));

//Routes middlewares
app.use('/api/brands', brandRoute);
app.use('/api/genders', genderRoute);
app.use('/api/perfums', perfumsRoute);
app.use('/imgPerfumes',express.static('imgPerfumes'));

app.listen(app.get('port'),()=> console.log('Working'));