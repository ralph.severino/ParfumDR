const mongoose = require('mongoose');

const genderSchema =  mongoose.Schema({
    name : {
        type: String,
        required:true,
        min: 2
    },
    creation_date : {
        type:Date,
        default:Date.now
    }
});



module.exports = mongoose.model('Gender',genderSchema);