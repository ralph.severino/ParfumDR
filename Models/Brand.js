const mongoose = require('mongoose');


const brandSchema =  mongoose.Schema({
    name:{
        type:String,
        min:2,
        max:255,
        required:true
    },
    creation_date:{
        type:Date,
        default:Date.now
    }    
})

module.exports = mongoose.model('Brand',brandSchema);