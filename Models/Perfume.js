const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PerfumeSchema = new Schema({
    name:{
        type:String,
        required: true,
        min:2,
        max:255
    },
    brand:{
        type: Schema.Types.ObjectId,
        ref: 'Brand',
        required:true
    },
    gender:{
        type: Schema.Types.ObjectId,
        ref: 'Gender',
        required:true
    },
    picture: {
        type:String,
        required:true
    },
    creation_date:{
        type:Date,
        default:Date.now
    }
});

const Perfume = mongoose.model('Perfume',PerfumeSchema);

module.exports = Perfume;